package json_deserializer;


import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import model.Flat;
import model.Person;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

//Дополнительное задание №10

public class FlatJsonDeserializer extends StdDeserializer<Flat> {
    protected FlatJsonDeserializer() {
        super(Flat.class);
    }


    @Override
    public Flat deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonToken token = jsonParser.nextToken();

        int number = -1;
        double square = -1;
        List<Person> personList = new ArrayList<>();

        while (token != JsonToken.END_OBJECT) {
            String currName = jsonParser.currentName();

            if ("number".equals(currName)) {
                token = jsonParser.nextToken();
                number = jsonParser.getIntValue();
            }

            else if ("square".equals(currName)) {
                token = jsonParser.nextToken();
                square = jsonParser.getDoubleValue();
            }

            else if ("personList".equals(currName)) {
                jsonParser.nextToken();
                token = jsonParser.nextToken();

                ObjectMapper objectMapper = new ObjectMapper();

                while (token != JsonToken.END_ARRAY) {
                    personList.add(objectMapper.readValue(jsonParser, Person.class));
                    token = jsonParser.nextToken();
                }
            }

            token = jsonParser.nextToken();
        }

        return new Flat(number, square, personList);
    }
}
