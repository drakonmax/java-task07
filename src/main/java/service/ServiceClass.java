package service;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class ServiceClass{

    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Кто-нибудь читает задания?
     "Предполагается, что до чтения массив уже создан, "
    */
    //Москвичёв М.М. 20.06.2020. Исправлено.
    public static int[] inStream(DataInputStream input, int n, int[] array) throws IOException {
        for (int i=0; i<n; i++)
            array[i] = input.readInt();
        return array;
    }

    public static void outStream(int[] numbers, DataOutputStream output) throws IOException {
        for (int i=0; i<numbers.length; i++)
            output.writeInt(numbers[i]);
    }

    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Не работает!
     У вас есть текстовый файл со строкой "1 2 3" это метод должен прочитать в массив int[]{1, 2, 3}
     Причем массив создавать не нужно, а нужно передать его как параметр.
    */
    //Москвичёв М.М. 20.06.2020. Исправлено.
    public static int[] inSymbolStream(Reader input, int [] res) throws IOException {
        try(BufferedReader arrRead = new BufferedReader(input)) {
            String[] numbers = arrRead.readLine().split(" ");
            for(int i = 0; i < res.length; i++){
                res[i] = Integer.parseInt(numbers[i]);
            }
            return res;
        }
    }

    /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
     Не работает! Нужно записать массив целых в текстовый файл через пробел, те из int[]{1,2,3} получить "1 2 3"
    */
    //Москвичёв М.М. 20.06.2020. Исправлено.
    public static void outSymbolStream(int[] symbols, Writer output) throws IOException {
        try(BufferedWriter arrWrite = new BufferedWriter(output)) {
            for(int temp: symbols){
                arrWrite.write(String.valueOf(temp));
                arrWrite.write(' ');
            }
        }
    }

    public static int[] inRandomAccessFile(RandomAccessFile input, int position, int size) throws IOException {
        if (position < 0)
            throw new IllegalArgumentException("NEGATIVE_POSITION");
        if (size < 0)
            throw new IllegalArgumentException("NEGATIVE_SIZE");
        input.seek(position);
        int[] array = new int[size];
        for (int i=0; i<size; i++)
            array[i] = input.readInt();
        return array;
    }

    public static List<File> fileExtension(File dir, String extension) throws IOException{
        if(!dir.isDirectory()){
            throw new IOException();
        }

        /* Филиппов А.В. 20.06.2020 Комментарий не удалять.
         Зачем вам else?
        */
        //Москвичёв М.М. 20.06.2020. Исправлено.
            List<File> list = new ArrayList<>();
            String suffix = "." + extension;

            for (File file: dir.listFiles()) {
                if (file.isFile()) {
                    String fileName = file.getName();

                    if (fileName.endsWith(suffix)){
                        list.add(file);
                    }
                }
            }
            return list;
    }

    //Дополнительное задание №5
    private static List<File> getFilesByPattern(File dir, Pattern pattern) {
        List<File> files = new ArrayList<>();

        for (File file: dir.listFiles()) {
            if (pattern.matcher(file.getName()).matches())
                files.add(file);

            if (file.isDirectory())
                files.addAll(getFilesByPattern(file, pattern));
        }

        return files;
    }


    public static List<File> getFilesByRegex(File dir, String regex) {
        if (!dir.isDirectory())
            throw new IllegalArgumentException();

        return regex == null?
                new ArrayList<>() :
                getFilesByPattern(dir, Pattern.compile(regex));
    }
}
