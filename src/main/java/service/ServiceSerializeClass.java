package service;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Flat;
import model.House;
import model.Person;


import java.io.*;
import java.nio.charset.Charset;
import java.util.List;

@JsonAutoDetect(creatorVisibility = JsonAutoDetect.Visibility.PROTECTED_AND_PUBLIC,
                fieldVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY,
                getterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY,
                setterVisibility = JsonAutoDetect.Visibility.PUBLIC_ONLY,
                isGetterVisibility = JsonAutoDetect.Visibility.PROTECTED_AND_PUBLIC)


public class ServiceSerializeClass{
    public static void serializeHouseToBinaryFile(File file, House house) throws IOException {
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(file))) {
            objectOutputStream.writeObject(house);
        }
    }

    public static House deserializeHouseFromBinaryFile(File file) throws IOException, ClassNotFoundException {
        House house = null;
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file))) {
            house = (House) objectInputStream.readObject();
        }
        return house;
    }

    //Дополнительное задание №7
    public static void serializeHouseToCsv(House house, File dir, Charset charset) throws IOException {
        if (!dir.isDirectory())
            throw new IllegalArgumentException();

        String sep = System.lineSeparator();

        String cadastralNumber = house.getCadastralNumber();
        String fileName = String.format("house_%s.csv",
                cadastralNumber == null?
                        "no_cadastral_number" : cadastralNumber.replace(':', '.'));

        try (BufferedWriter out =
                     new BufferedWriter(new OutputStreamWriter(new FileOutputStream(new File(dir, fileName)), charset))) {
            out.write("Данные о доме;;" + sep);
            out.write(String.format("Кадастровый номер;%s;" + sep,
                    cadastralNumber == null? "" : cadastralNumber));
            out.write(String.format("Адрес;\"%s\";" + sep, house.getAddress()));
            out.write("Старший по дому;");

            Person head = house.getHousekeeper();
            if (head != null) {
                out.write(head.getLastName());
                out.write(" ");
                out.write(head.getFirstName());

                String patronymicName = head.getPatronymicName();
                if (patronymicName != null) {
                    out.write(' ');
                    out.write(patronymicName);
                }
            }

            out.write(";" + sep);
            out.write(";;" + sep);
            out.write("Данные о квартирах;;" + sep);
            out.write("№;\"Площадь, кв. м\";Владельцы" + sep);

            for (Flat flat: house.getFlats()) {
                out.write(String.format("%d;\"%s\";\"", flat.getNumber(),
                        Double.toString(flat.getSquare()).replace('.', ',')));

                List<Person> owners = flat.getPersonList();
                for (int i = 0; i < owners.size(); i++) {
                    Person owner = owners.get(i);

                    out.write(String.format("%s %c.", owner.getLastName(), owner.getFirstName().charAt(0)));

                    String patronymicName = owner.getPatronymicName();
                    if (patronymicName != null) {
                        out.write(String.format(" %c.", patronymicName.charAt(0)));
                    }

                    if (i < owners.size() - 1)
                        out.write(", ");
                }

                out.write('"' + sep);
            }
        }
    }


    public static String serializeHouseToJsonString(House house) throws IOException {
        return new ObjectMapper().writeValueAsString(house);
    }

    public static House deserializeHouseFromJsonString(String json) throws IOException {
        return new ObjectMapper().readValue(json, House.class);
    }
}
