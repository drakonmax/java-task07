package model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import json_deserializer.HouseJsonDeserializer;
import json_serializer.HouseJsonSerializer;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@JsonSerialize(using = HouseJsonSerializer.class)
@JsonDeserialize(using = HouseJsonDeserializer.class)

public class House implements Serializable {
    private String cadastralNumber;
    private String address;
    private Person housekeeper;
    private List<Flat> flats;

    public House(){

    }

    public House(String cadastralNumber, String address, Person housekeeper, List<Flat> flats) {
        this.cadastralNumber = cadastralNumber;
        this.address = address;
        this.housekeeper = housekeeper;
        this.flats = flats;
    }

    public String getCadastralNumber() {
        return cadastralNumber;
    }

    public void setCadastralNumber(String cadastralNumber) {
        this.cadastralNumber = cadastralNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getHousekeeper() {
        return housekeeper;
    }

    public void setHousekeeper(Person housekeeper) {
        this.housekeeper = housekeeper;
    }

    public List<Flat> getFlats() {
        return flats;
    }

    public void setFlats(List<Flat> flats) {
        this.flats = flats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return Objects.equals(cadastralNumber, house.cadastralNumber) &&
                Objects.equals(address, house.address) &&
                Objects.equals(housekeeper, house.housekeeper) &&
                Objects.equals(flats, house.flats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cadastralNumber, address, housekeeper, flats);
    }
}
