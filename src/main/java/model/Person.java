package model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import json_deserializer.PersonJsonDeserializer;
import json_serializer.PersonJsonSerializer;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@JsonSerialize(using = PersonJsonSerializer.class)
@JsonDeserialize(using = PersonJsonDeserializer.class)

public class Person implements Serializable {
    private String lastName;
    private String firstName;
    private String patronymicName;
    private Date birthday;

    public Person(){

    }

    public Person(String firstName, String patronymicName, String lastName,  Date birthday) {
        this.firstName = firstName;
        this.patronymicName = patronymicName;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymicName() {
        return patronymicName;
    }

    public void setPatronymicName(String patronymicName) {
        this.patronymicName = patronymicName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(lastName, person.lastName) &&
                Objects.equals(firstName, person.firstName) &&
                Objects.equals(patronymicName, person.patronymicName) &&
                Objects.equals(birthday, person.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(lastName, firstName, patronymicName, birthday);
    }
}
