package model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import json_deserializer.FlatJsonDeserializer;
import json_serializer.FlatJsonSerializer;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@JsonSerialize(using = FlatJsonSerializer.class)
@JsonDeserialize(using = FlatJsonDeserializer.class)

public class Flat implements Serializable {
    private Integer number;
    private double square;
    private List<Person> personList;

    public Flat(){

    }

    public Flat(Integer number, double square, List<Person> personList) {
        this.number = number;
        this.square = square;
        setPersonList(personList);
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public double getSquare() {
        return square;
    }

    public void setSquare(double square) {
        this.square = square;
    }

    public List<Person> getPersonList() {
        return personList;
    }

    public void setPersonList(List<Person> personList) {

        if (personList == null)
        this.personList = new ArrayList<>();

        else
            this.personList = personList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flat flat = (Flat) o;
        return Double.compare(flat.square, square) == 0 &&
                Objects.equals(number, flat.number) &&
                Objects.equals(personList, flat.personList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, square, personList);
    }
}
