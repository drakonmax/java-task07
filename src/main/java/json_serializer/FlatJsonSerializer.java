package json_serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import model.Flat;
import model.Person;

import java.io.IOException;

//Дополнительное задание №10

public class FlatJsonSerializer extends StdSerializer<Flat> {
    protected FlatJsonSerializer() {
        super(Flat.class);
    }


    @Override
    public void serialize(Flat flat, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();

        jsonGenerator.writeNumberField("number", flat.getNumber());
        jsonGenerator.writeNumberField("square", flat.getSquare());

        jsonGenerator.writeArrayFieldStart("personList");
        for (Person person: flat.getPersonList())
            jsonGenerator.writeObject(person);
        jsonGenerator.writeEndArray();

        jsonGenerator.writeEndObject();
    }
}