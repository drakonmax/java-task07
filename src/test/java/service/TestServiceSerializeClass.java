package service;

import model.Flat;
import model.House;
import model.Person;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

public class TestServiceSerializeClass {
    private static Date date1 = new Date();
    private static Date date2 = new Date();
    private static Date date3 = new Date();

    @BeforeAll
    static void initCalendar(){
        Calendar cal = Calendar.getInstance();

        cal.set(2000, Calendar.AUGUST, 1);
        date1 = cal.getTime();

        cal.set(1975, Calendar.APRIL, 27);
        date2 = cal.getTime();

        cal.set(1975, Calendar.APRIL, 30);
        date3 = cal.getTime();
    }

    @TempDir
    static File tempDir;
    static Person person1, person2, person3;
    static Flat flat1, flat2, flat3, flat4;
    static House house1;

    @BeforeAll
    static void initModels(){
        person1 = new Person("Иван", "Иванович", "Петров", date3);
        person2 = new Person("Анастасия", "Сергеевна", "Иванова", date1);
        person3 = new Person("Агафья", "Фёдоровна", "Воронцова", date2);
        flat1 = new Flat(1, 60.25, Arrays.asList(person1, person2));
        flat2 = new Flat(2, 40, Collections.singletonList(person3));
        flat3 = new Flat(10, 100, Arrays.asList(person1, person2, person3));
        flat4 = new Flat(9, 25, null);
        house1 = new House("55:36:050208:11906", "ул. Кого-то там, 13", person3,
                Arrays.asList(flat1, flat2, flat3, flat4));
    }


    @Test
    void testSerializeToCsv() throws IOException {

        ServiceSerializeClass.serializeHouseToCsv(house1, tempDir, StandardCharsets.UTF_8);

        File file = new File(tempDir, "house_55.36.050208.11906.csv");

        assertTrue(file.exists());

        String expected = "Данные о доме;;\n" +
                "Кадастровый номер;55:36:050208:11906;\n" +
                "Адрес;\"ул. Кого-то там, 13\";\n" +
                "Старший по дому;Воронцова Агафья Фёдоровна;\n" +
                ";;\n" +
                "Данные о квартирах;;\n" +
                "№;\"Площадь, кв. м\";Владельцы\n" +
                "1;\"60,25\";\"Петров И. И., Иванова А. С.\"\n" +
                "2;\"40,0\";\"Воронцова А. Ф.\"\n" +
                "10;\"100,0\";\"Петров И. И., Иванова А. С., Воронцова А. Ф.\"\n" +
                "9;\"25,0\";\"\"\n";
        StringBuilder actual = new StringBuilder();

        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8))) {
            String line;

            while ((line = br.readLine()) != null)
                actual.append(line).append('\n');
        }

        assertEquals(expected, actual.toString());
        assertThrows(IllegalArgumentException.class,
                        () -> ServiceSerializeClass.serializeHouseToCsv(house1, file, StandardCharsets.UTF_8));
    }



    @Test
    void testFileSerializeAndDeserializeHouse() throws IOException, ClassNotFoundException {
        List<Flat> flatList = new ArrayList<>();
        List<Person> personList = new ArrayList<>();
        Collections.addAll(personList, new Person("name","familyName","lastName",date1),
                new Person("name1","familyName1","lastName1",date2));
        List<Person> personList1 = new ArrayList<>();
        Collections.addAll(personList, new Person("name2","familyName2","lastName2",date1),
                new Person("name1","familyName1","lastName1",date3));
        Collections.addAll(flatList,new Flat(12,30,personList),new Flat(10,33,personList1));
        House house = new House("1234","5th avenue",
                new Person("name","familyName","lastName",date1),flatList);
        File file = new File("test.txt");
        ServiceSerializeClass.serializeHouseToBinaryFile(file,house);
        assertTrue(file.exists());
        House house1 = ServiceSerializeClass.deserializeHouseFromBinaryFile(file);
        assertEquals(house,house1);
    }
    @Test
    void testSerializeAndDeserializeHouseToJsonString() throws IOException {
        String house1Json = ServiceSerializeClass.serializeHouseToJsonString(house1);
        assertEquals(house1, ServiceSerializeClass.deserializeHouseFromJsonString(house1Json));
    }

}
