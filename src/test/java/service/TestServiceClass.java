package service;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static service.MockFactory.getDirMock;
import static service.MockFactory.getFileMock;

public class TestServiceClass {

    @TempDir
    static File tempDir;
    static File rootDir, emptyDir, textDir, binDir, imagesDir, imagesPngDir, makefile1, makefile2;
    static File uselessTxt, essayDocx, oneBin, catTiff, dogTiff, logoPng;
    static final Comparator<File> mockFileComparator = Comparator.comparing(File::getName);


    @BeforeAll
    static void initMocks() {
        makefile1 = getFileMock("Makefile", "~/");
        makefile2 = getFileMock("Makefile", "~/text/");
        uselessTxt = getFileMock("useless.txt", "~/text/");
        essayDocx = getFileMock("essay.docx", "~/text/");
        oneBin = getFileMock("1.bin", "~/bin/");
        catTiff = getFileMock("cat.tiff", "~/images/");
        dogTiff = getFileMock("dog.tiff", "~/images/");
        logoPng = getFileMock("logo.png", "~/images/png/");

        emptyDir = getDirMock("empty", "~/");
        textDir = getDirMock("text", "~/", makefile2, uselessTxt, essayDocx);
        binDir = getDirMock("bin", "~/", oneBin);
        imagesPngDir = getDirMock("png", "~/images/", logoPng);
        imagesDir = getDirMock("images", "~/", catTiff, imagesPngDir, dogTiff);
        rootDir = getDirMock("~", "", makefile1, emptyDir, textDir, binDir, imagesDir);

//      ~/
//      ├ Makefile
//      ├ empty/
//      ├ text/
//      | ├ Makefile
//      | ├ useless.txt
//      | └ essay.docx
//      ├ bin/
//      | └ 1.bin
//      └ images/
//        ├ cat.tiff
//        ├ png/
//        | └ logo.png
//        └ dog.tiff
    }

    @Test
    void testWriteAndReadIntArray() throws IOException {
        int[] array = {1, 2, 3, 4, 5, 6, 7};
        int[] emptyArray = new int[7];

        File file = new File("test1.txt");
        DataOutputStream outputStream = new DataOutputStream(new FileOutputStream("test1.txt"));
        DataInputStream inputStream = new DataInputStream(new FileInputStream("test1.txt"));
        ServiceClass.outStream(array, outputStream);
        assertTrue(file.exists());
        assertEquals(7, file.length() / 4);
        assertArrayEquals(array, ServiceClass.inStream(inputStream, 7, emptyArray));
    }

    @Test
    void testWriteAndReadIntArrayWithWriterAndReader() throws IOException {

        CharArrayWriter out = new CharArrayWriter(16);
        int [] arr = new int[]{1,2,3};
        int [] resultArr = new int[3];
        ServiceClass.outSymbolStream(arr, out);
        CharArrayReader in = new CharArrayReader(out.toCharArray());
        int [] res = ServiceClass.inSymbolStream(in, resultArr);
        assertArrayEquals(arr, res);
    }

    @Test
    void testReadIntArrayFromRandomAccessFile() throws IOException {
        File file = new File(tempDir, "test.txt");
        try (RandomAccessFile input = new RandomAccessFile(file, "rw")){
        for (int i=0; i<10; i++){
            input.writeInt(i+2);
        }

        assertArrayEquals(new int[]{2, 3, 4, 5, 6, 7, 8, 9, 10, 11},
                ServiceClass.inRandomAccessFile(input,0,10));
        assertArrayEquals(new int[]{3, 4, 5},
                ServiceClass.inRandomAccessFile(input,4,3));
        try{
            ServiceClass.inRandomAccessFile(input, -7, 2);
        }
        catch (IllegalArgumentException e){
                assertEquals("NEGATIVE_POSITION",e.getMessage());
            }
        try{
            ServiceClass.inRandomAccessFile(input, 2, -7);
        }
        catch (IllegalArgumentException e){
                assertEquals("NEGATIVE_SIZE",e.getMessage());
            }
        }
    }

    @Test
    void testFileExtension() throws IOException {

        File file = File.createTempFile("test",".txt", tempDir);
        File file1 = File.createTempFile("test1",".java", tempDir);
        File file2 = File.createTempFile("test2",".java", tempDir);

        List<File> res = ServiceClass.fileExtension(tempDir,"java");

        assertEquals(Arrays.asList(file1,file2), res);
    }

    //Дополнительное задание 11

    @Test
    void testGetFilesByRegex() {
        List<File> files1 = ServiceClass.getFilesByRegex(textDir, ".*txt");
        List<File> files2 = ServiceClass.getFilesByRegex(rootDir, "\\d.bin");
        List<File> files3 = ServiceClass.getFilesByRegex(rootDir, "^[a-z]*\\.[a-z]*x.?$");
        List<File> files4 = ServiceClass.getFilesByRegex(rootDir, "[a-z]*es[a-z\\.]*");
        List<File> files5 = ServiceClass.getFilesByRegex(rootDir, "[\\w\\.]*");
        List<File> files6 = ServiceClass.getFilesByRegex(textDir, null);

        files3.sort(mockFileComparator);
        files4.sort(mockFileComparator);
        files5.sort(Comparator.comparing(File::getAbsolutePath));

        assertEquals(Collections.singletonList(uselessTxt), files1);
        assertEquals(Collections.singletonList(oneBin), files2);
        assertEquals(Arrays.asList(essayDocx, uselessTxt), files3);
        assertEquals(Arrays.asList(essayDocx, imagesDir, uselessTxt), files4);
        assertEquals(
                Arrays.asList(
                        makefile1, binDir, oneBin, emptyDir, imagesDir, catTiff, dogTiff, imagesPngDir, logoPng,
                        textDir, makefile2, essayDocx, uselessTxt), files5);
        assertEquals(Collections.emptyList(), files6);

    }


    @Test
    void testGetFilesByRegexNotADirectory() {
        assertThrows(IllegalArgumentException.class, () -> ServiceClass.getFilesByRegex(essayDocx, ".*txt"));
    }
}
